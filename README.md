# Add timing to your commits

A small addition to [CaptainHook](https://packagist.org/captainhook/captainhook) that will add the time since your last commit to your commitmessage

That way you can use a different tool to extract the times you spent on a project from the commitmessages

To be sure that the time is added to each commit there is also a check that can be used to verify that a time is added.

[![GitLab-CI Bild-Status](https://gitlab.com/heiglandreas/githook_addtime/badges/rainy-picknick/build.svg)](https://gitlab.com/heiglandreas/githook_addtime)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/heiglandreas/githook_addtime/badges/quality-score.png?b=rainy-picknick)](https://scrutinizer-ci.com/g/heiglandreas/githook_addtime/?branch=rainy-picknick)



## Installation

Install this package using composer

```bash
composer require --dev heiglandreas/captainhook_addtime
```

## Usage


### Adding time

In your `captainhook.json` file you can then add this hook to your `prepare-commit-msg`-hooks:

```json
{
  "prepare-commit-msg" : {
    "enabled" : true,
    "actions" : [{
      "action" : "\\Org_Heigl\\CaptainHook\\Hooks\\AddTime\\AddTimeAction",
      "options" : {
        "fuzziness": "\\Org_Heigl\\CaptainHook\\Hooks\\AddTime\\Fuzzier\\TenMinutesCeiling"
      }
    }]
  }
}
```

This will round up to the next 10 minutes.

### Verifying time is added

To make sure that each commit contains a time you can add this to your `captainhook.json`-file:

```json
{
  "commit-msg": {
    "enabled": true,
    "actions": [{
      "action": "\\Org_Heigl\\CaptainHook\\Hooks\\AddTime\\EnsureTime",
      "options": {}
    }]
  }
}
```

This will require each commit-message to contain something like the following on one line:

```
Time: 12h15m
```

There can be whitespace before or after the content but the 'h' and 'm' needs to be
lower case and there must not be any whitespace in that time-information.

You can ommit the hours **or** the minutes. (Ommiting both doesn't
really make sense (-; )