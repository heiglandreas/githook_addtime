<?php

declare(strict_types=1);

/**
 * Copyright Andrea Heigl <andreas@heigl.org>
 *
 * Licenses under the MIT-license. For details see the included file LICENSE.md
 */

namespace Org_Heigl\CaptainHook\Hooks\AddTime\CommitMessage;


use SebastianFeldmann\Git\CommitMessage;

class CommitMessageHandlerFactory
{
    public function fromMessage(CommitMessage $message) : CommitMessageHandler
    {
        return new CommitMessageHandler($message);
    }
}