<?php

declare(strict_types=1);

/**
 * Copyright Andrea Heigl <andreas@heigl.org>
 *
 * Licenses under the MIT-license. For details see the included file LICENSE.md
 */

namespace Org_Heigl\CaptainHook\Hooks\AddTime;

use CaptainHook\App\Config;
use CaptainHook\App\Console\IO;
use CaptainHook\App\Hook\Action;
use function preg_match;
use RuntimeException;
use SebastianFeldmann\Git\Repository;

class EnsureTime implements Action
{
    private $timeString = '/^\s*Time: (((\d+)h((\d+)m)?)|((\d+)m))\s*$/';
    /**
     * Executes the action.
     *
     * @param  \CaptainHook\App\Config $config
     * @param  \CaptainHook\App\Console\IO $io
     * @param  \SebastianFeldmann\Git\Repository $repository
     * @param  \CaptainHook\App\Config\Action $action
     * @throws \Exception
     */
    public function execute(Config $config, IO $io, Repository $repository, Config\Action $action) : void
    {
        $commitMessage = $repository->getCommitMsg();

        foreach ($commitMessage->getLines() as $line) {
            if (preg_match($this->timeString, $line)) {
                return;
            }
        }

        throw new RuntimeException(sprintf(
            'No Time-String matching the expectation was found in the commit message.'
        ));
    }
}