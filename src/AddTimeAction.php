<?php

declare(strict_types=1);

/**
 * Copyright Andrea Heigl <andreas@heigl.org>
 *
 * Licenses under the MIT-license. For details see the included file LICENSE.md
 */

namespace Org_Heigl\CaptainHook\Hooks\AddTime;

use CaptainHook\App\Config;
use CaptainHook\App\Console\IO;
use CaptainHook\App\Hook\Action;
use DateTimeImmutable;
use Exception;
use InvalidArgumentException;
use Org_Heigl\CaptainHook\Hooks\AddTime\CommitMessage\CommitMessageHandlerFactory;
use Org_Heigl\CaptainHook\Hooks\AddTime\Formatter\TimeDifference;
use SebastianFeldmann\Git\Repository;

class AddTimeAction implements Action
{
    /**
     * Executes the action.
     *
     * @param  \CaptainHook\App\Config $config
     * @param  \CaptainHook\App\Console\IO $io
     * @param  \SebastianFeldmann\Git\Repository $repository
     * @param  \CaptainHook\App\Config\Action $action
     * @throws \Exception
     */
    public function execute(Config $config, IO $io, Repository $repository, Config\Action $action) : void
    {
        $fuzziness = null;
        $options = $action->getOptions();
        if ($options->get('fuzziness') != null) {
            $class = $options->get('fuzziness');
            $fuzziness = new $class();
        }

        $formatter = new AddTime(
            new TimeDifference($fuzziness),
            new CommitMessageHandlerFactory()
        );

        $formatter->execute($config, $io, $repository, $action);
    }
}