<?php
/**
 * Copyright Andrea Heigl <andreas@heigl.org>
 *
 * Licenses under the MIT-license. For details see the included file LICENSE.md
 */

namespace Org_Heigl\CaptainHook\Hooks\AddTimeTest;

use CaptainHook\App\Config;
use CaptainHook\App\Config\Action;
use CaptainHook\App\Console\IO;
use CaptainHook\App\Console\IOUtil;
use Org_Heigl\CaptainHook\Hooks\AddTime\EnsureTime;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use SebastianFeldmann\Git\CommitMessage;
use SebastianFeldmann\Git\Repository;

class EnsureTimeTest extends TestCase
{
    /** @var Config & MockObject */
    private $config;

    /** @var IO & MockObject */
    private $io;

    /** @var Repository & MockObject */
    private $repo;

    /** @var Action & MockObject */
    private $action;

    public function setup()
    {
        $this->config = self::getMockBuilder(Config::class)->disableOriginalConstructor()->getMock();
        $this->io     = self::getMockBuilder(IO::class)->disableOriginalConstructor()->getMock();
        $this->repo   = self::getMockBuilder(Repository::class)->disableOriginalConstructor()->getMock();
        $this->action = self::getMockBuilder(Action::class)->disableOriginalConstructor()->getMock();

    }

    /**
     * @expectedException RuntimeException
     * @dataProvider executeWithoutValidTimeContentProvider
     */
    public function testExecuteWithoutTimeContent(string $content)
    {
        $message = new CommitMessage($content);

        $this->repo->method('getCommitMsg')->willReturn($message);

        $tester = new EnsureTime();

        $tester->execute($this->config, $this->io, $this->repo, $this->action);
    }

    /**
     * @dataProvider executeWithValidTimeContentProvider
     */
    public function testExecuteWithTimeContent(string $content)
    {
        $message = new CommitMessage($content);

        $this->repo->method('getCommitMsg')->willReturn($message);

        $tester = new EnsureTime();

        $result = $tester->execute($this->config, $this->io, $this->repo, $this->action);

        self::assertNull($result);
    }



    public function executeWithoutValidTimeContentProvider() : array
    {
        return [
            ['Test'],
            ["test\n\n#Time: 1h15mtest"],
            ["test\n\nTime: 1h15mtest"],
            ["test\n\nTime: 1h15stest"],
            ["test\n\nTime: 1s"],
            ["test\n\nTime: 1m12h"],
        ];
    }

    public function executeWithValidTimeContentProvider() : array
    {
        return [
            ['Time: 12h15m'],
            ["test\n\nTime: 1h15m"],
            ["test\n\nTime: 1h"],
            ["test\n\nTime: 7m"],
            ["test\n\n Time: 1h15m"],
            ["test\n\n Time: 1h15m  "],
        ];
    }
}
