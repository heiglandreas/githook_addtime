<?php
/**
 * Copyright Andrea Heigl <andreas@heigl.org>
 *
 * Licenses under the MIT-license. For details see the included file LICENSE.md
 */

namespace Org_Heigl\CaptainHook\Hooks\AddTimeTest\CommitMessage;

use Org_Heigl\CaptainHook\Hooks\AddTime\CommitMessage\CommitMessageHandler;
use Org_Heigl\CaptainHook\Hooks\AddTime\CommitMessage\CommitMessageHandlerFactory;
use PHPUnit\Framework\TestCase;
use SebastianFeldmann\Git\CommitMessage;

class CommitMessageHandlerFactoryTest extends TestCase
{

    public function testFromMessage()
    {
        $factory = new CommitMessageHandlerFactory();

        $message = new CommitMessage('Foo');

        $handler = $factory->fromMessage($message);

        self::assertInstanceOf(CommitMessageHandler::class, $handler);
    }
}
